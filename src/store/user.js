import firebase from 'firebase';

class User {
    constructor(id) {
        this.id = id;
    }
}

export default {
    state: {
        user: null,
    },
    actions: {
        async registerUser({ commit }, { email, password }) {
            commit('clearError');
            commit('setLoading', true);
            try {
                const user = await firebase.auth().createUserWithEmailAndPassword(email, password);
                commit('initUserId', new User(user.user.uid));
                commit('setLoading', false);
            } catch (error) {
                commit('setLoading', false);
                commit('setError', error.message);
                throw error;
            }
        },
        async loginUser({ commit }, { email, password }) {
            commit('clearError');
            commit('setLoading', true);
            try {
                const user = await firebase.auth().signInWithEmailAndPassword(email, password);
                commit('initUserId', new User(user.user.uid));
                commit('setLoading', false);
            } catch (error) {
                commit('setLoading', false);
                commit('setError', error.message);
                throw error;
            }
        },
        autoLoginUser({ commit }, payload) {
            commit('initUserId', new User(payload.uid));
        },
        logoutUser({ commit }) {
            firebase.auth().signOut();
            commit('initUserId', null);
        },
    },
    getters: {
        user(state) {
            return state.user;
        },
        isUserLoggedIn(state) {
            return state.user !== null;
        },
    },
    mutations: {
        initUserId(state, payload) {
            state.user = payload;
        },
    },
};
