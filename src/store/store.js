import Vue from 'vue';
import Vuex from 'vuex';
import user from './user';
import status from './status';
import drinkData from './drinkData';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    actions: {},
    getters: {},
    mutations: {},
    modules: {
        user, status, drinkData,
    },

});
