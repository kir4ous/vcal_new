import Vue from 'vue';
import Router from 'vue-router';
import AuthGuard from './authguard';
import defaultLayout from '@/layouts/defaultLayout.vue';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            component: defaultLayout,
            children: [
                {
                    path: '',
                    component: () => import('@/components/calendar/calendar'),
                },
                {
                    path: '/stats',
                    component: () => import('@/components/stats'),
                    beforeEnter: AuthGuard,
                },
                {
                    path: '/norms',
                    component: () => import('@/components/norms'),
                },
                {
                    path: '/contactus',
                    component: () => import('@/components/contactus'),
                },
                {
                    path: '/login',
                    component: () => import('@/components/auth/login'),
                },
                {
                    path: '/registration',
                    component: () => import('@/components/auth/registration'),
                },
            ],
        },
    ],
});
