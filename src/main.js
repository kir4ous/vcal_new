import Vue from 'vue';
import Vuetify from 'vuetify';
import firebase from 'firebase';
import App from './App.vue';
import router from './router';
import store from './store/store';
import config from './config';

import 'vuetify/dist/vuetify.min.css';
import '@mdi/font/css/materialdesignicons.css';

import axios from 'axios';

window.axios = axios;

Vue.use(Vuetify, {
    iconfont: 'md' || 'mdiSvg' || 'mdi',
    theme: {
        primary: '#880E4F',
    },
});
Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App),
    created() {
        firebase.initializeApp(config);
        firebase.analytics();
        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                this.$store.dispatch('autoLoginUser', user);
            }
        });
    },
}).$mount('#app');
